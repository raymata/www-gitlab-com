---
layout: markdown_page
title: "Demo & Test data working group"
description: "TBD"
canonical_path: "/company/team/structure/working-groups/demo-test-data/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value           |
|-----------------|-----------------|
| Date Created    | 2022-01-19 |
| Target End Date | 2023-04-30 |
| Slack           | [#wg_demo-test-data](https://gitlab.slack.com/archives/C02M7GX1SBE) (only accessible from within the company) |
| Google Doc      | [Working Group Agenda](https://docs.google.com/document/d/1XmTGP1pNBDaC6LduW8rygYBdd8BrS5el2zjGvI7Dtyc/edit#heading=h.epyavtxljcb2) |
| Issues      | [Issue List](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=milestone&state=opened&label_name%5B%5D=wg_demo-test-data&first_page_size=20) / [Issue Board](https://gitlab.com/gitlab-org/gitlab/-/boards/3766722) |
| Overview & Status | See [Board](https://gitlab.com/gitlab-org/gitlab/-/boards/3766722) |

## Business Goal

The efficiency of both demo and test data is key to moving our business faster. We have fragmented locations where these data resides and are provisioned. There is also a knowledge gap between what is used in technical sales and what is used in test and validation.  

We will benefit from tooling efficiency together and broaden visiblity of demo data in the field and test data in Engineering.

### Exit Criteria (0% completed)

1. Improve built in demo data via sample project templates
1. Delivery of first working demo data usable by SAs in the field
1. Delivery of add-on automation to update demo data via GitLab Triage
1. Implement automated test data provisioning for test environments  
1. Identify differences and gaps between demo and test data 
1. Publish Demo/Test data structure in the handbook along with identified future DRIs
1. Provision first 3 versions of test/demo data on all developer used test environments (staging,review apps and GDK)
1. Outline plan for Demo data on Reference Architecture playbook to enhance team enablement

### Roles and Responsibilities

| Working Group Role    | Person                 | Title                             |
|-----------------------|------------------------|-----------------------------------|
| Facilitator           | Grant Young            | Staff Software Engineer in Test   |
| Stakeholder           | Tim Poffenbarger       | Senior Manager, Solutions Architects |
| Stakeholder           | Tanya Pazitny          | Director of Quality               |
| Stakeholder           | Vincy Wilson           | Senior Manager, Quality Engineering |
| Executive Sponsor     | Mek Stittri            | VP of Quality                     |
| Member                | Marshall Cottrell.     | Principal, Strategy and Operations (Technical) |   
| Member                | Tim Zallmann           | Senior Director of Engineering    |
| Member                | Mark Wood              | Senior Product Manager            |
| Member                | Sameer Kamani          | Staff Federal Solution Architect  |
| Member                | Joe Randazzo           | Solutions Architect               |
| Member                | Darwin Sanoy           | Staff Solutions Architect         |
| Member                | Dan Davison            | Staff  Software Engineer in Test  |
